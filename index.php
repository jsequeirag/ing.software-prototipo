<?php

include_once "conexion.php";

$query = 'SELECT * FROM categoria';
$resultado = conexionCover()->prepare($query);
$resultado->execute();
$categorias = $resultado->fetchAll();

if ($_GET) {
}




?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css" rel="stylesheet" />

    <link href="index.css" rel="stylesheet" />

</head>



<body class="bg-slaygray">
    <header>
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-light bg-white">

            <div class="container-fluid ">
                <div class="container mt-2 mb-2 me-2 ms-2">
                    <a class="navbar-brand" href="#">
                        <img src="https://mdbootstrap.com/img/logo/mdb-transaprent-noshadows.png" height="30" alt="" loading="lazy" />
                    </a>
                </div>
                <div class="d-flex justify-content-start" style="width:200%;">
                    <form method="GET" class=" input-group w-auto ">
                        <select name='categoria' class="form-select me-1" aria-label="Default select example" style="height:45px;width:300px" ;>
                            <option name='categoria' value="todo" ?>Todo</option>
                            <?php
                            foreach ($categorias as $categoria) :
                            ?>
                                <option value="<?php echo $categoria['nombre'] ?>"><?php echo $categoria['nombre'] ?></option>

                            <?php endforeach ?>

                        </select>
                        <input type="search" name='search' class="form-control" placeholder="Buscar" aria-label="Search" style="height:45px;width:300px" />
                        <button class="btn btn-outline-primary" type="submit" data-mdb-ripple-color="dark">
                            Search
                        </button>
                    </form>
                </div>
                <div class="btn-group me-2 ms-2" role="group">
                    <a href="login.php">
                        <button type="button" class="btn btn-primary">Login</button>
                    </a>
                </div>
            </div>
        </nav>
        <!-- Navbar -->
        <?php
        if ($_GET) :
        ?>

        <?php
        else :
        ?>

            <!-- Jumbotron -->
            <div class="p-5 text-center bg-light  text-white" style="background-image:url('header.png') ;">

                <h1 class="mb-3">Bienvenido a la mejor tienda en linea!</h1>
                <h4 class="mb-3">No tienes cuenta</h4>
                <a class="btn btn-primary" href="" role="button">Registrarme</a>
            </div>
            <!-- Jumbotron -->

        <?php
        endif;
        ?>
    </header>
    <main class="main  ">

        <?php
        if ($_GET) :
        ?>
            <div class="container scrollcontainer border  mt-3 mb-3">


                <div class="row">
                    <div class="col-md-12">
                        <!-- Spied element -->
                        <div data-mdb-spy="scroll" data-mdb-target="#scrollspy1" data-mdb-offset="0" class="scrollspy-example d-flex flex-wrap flex-row">

                            <div class="card mt-3 mb-3 me-4 ms-4 border" style="width: 16.5rem; height:22.5rem;">
                                <img src="https://mdbootstrap.com/img/new/standard/nature/184.jpg" width: 17rem; height:17rem; class="card-img-top" alt="..." />
                                <div class="card-body">
                                    <div class="d-flex row">
                                        <div>
                                            <h5 class="card-title">Titulo</h5>
                                        </div>
                                        <div class="card-title">
                                            <h6 class="card-title">Precio</h6>
                                        </div>
                                    </div>
                                    <p class="card-text">
                                        Descripcion...
                                    </p>
                                    <a href="articulo.php" class="btn btn-primary">Ver</a>
                                </div>
                            </div>

                            <!-- Spied element -->
                        </div>

                    </div>

                </div>
            </div>
        <?php
        else :
        ?>


            <div class="container d-flex flex-row d-flex justify-content-between flex-wrap  ">
                <div class="card me-1 mt-5   mb-1 ms-1" style="width: 18rem; height:19rem">
                    <img src="img/accesorios.jpg" style="width: 12rem; height:12rem; " class="card-img-top align-self-center" alt="..." />
                    <div class="card-body">
                        <h5 class="card-title">Accesesorios</h5>
                        <p class="card-text">

                        </p>
                        <a href="#!" class="btn btn-primary">Ver</a>
                    </div>
                </div>
                <div class="card me-1 mt-5   mb-1 ms-1" style="width: 18rem; height:19rem">
                    <img src="img/pantalones.jpg" style="width: 12rem; height:12rem; " class="card-img-top align-self-center" alt="..." />
                    <div class="card-body">
                        <h5 class="card-title">Pantalones</h5>
                        <p class="card-text">

                        </p>
                        <a href="#!" class="btn btn-primary">Ver</a>
                    </div>
                </div>
                <div class="card me-1 mt-5   mb-1 ms-1" style="width: 18rem; height:19rem">
                    <img src="img/camisa.jpg" style="width: 12rem; height:12rem; " class="card-img-top align-self-center" alt="..." />
                    <div class="card-body">
                        <h5 class="card-title">Camisas</h5>
                        <p class="card-text">

                        </p>
                        <a href="#!" class="btn btn-primary">Ver</a>
                    </div>
                </div>
                <div class="card me-1 mt-5   mb-1 ms-1" style="width: 18rem; height:19rem">
                    <img src="img/zapatos.jpg" style="width: 12rem; height:12rem; " class="card-img-top align-self-center" alt="..." />
                    <div class="card-body">
                        <h5 class="card-title">Zapatos</h5>
                        <p class="card-text">

                        </p>
                        <a href="#!" class="btn btn-primary">Ver</a>
                    </div>
                </div>
            </div>
            <div class="container mt-5 mb-5 d-flex flex-row d-flex justify-content-between flex-wrap">

                <div id="carouselExampleSlidesOnly" class="carousel slide  mt-1  mb-1" data-mdb-ride="carousel" style="width:500px; height:500px">
                    <div class="carousel-inner ">
                        <div class="carousel-item active bg-white ">
                            <img src="img/adidas.jpg" style="width:500px; height:500px " class="card-img-top " alt="..." />
                        </div>
                        <div class="carousel-item bg-white ">
                            <img src="img/etnies.jpg" style="width:500px; height:500px " alt="..." />
                        </div>
                        <div class="carousel-item bg-white  ">
                            <img src="img/jordan.jpg" style="width:500px; height:500px " alt="..." />
                        </div>
                    </div>
                </div>
                <div class="card me-1 mt-1 mb-1 " style="width: 22rem; height:500px">
                    <img src="img/zjordan.jpg" style="width: 500; height:550; " class="card-img-top align-self-center" alt="..." />
                    <div class="card-body">
                        <div d-flex>
                            <h5 class="card-title">Adidas Samba</h5>
                        </div>
                        <p class="card-text">

                        </p>
                        <a href="#!" class="btn btn-primary">ver</a>
                    </div>
                </div>
                <div class="card me-1 mt-1 mb-1 " style="width: 22rem; height:500px">
                    <img src="img/zjordan1.jpg" style="width: 500; height:550; " class="card-img-top align-self-center" alt="..." />
                    <div class="card-body">
                        <h5 class="card-title">Nike Turbo</h5>
                        <p class="card-text">

                        </p>
                        <a href="#!" class="btn btn-primary">ver</a>
                    </div>
                </div>

            </div>

            <div class="container">
                <h1>Mas vendido</h1>
                <div id="carouselMultiItemExample" class="carousel slide carousel-dark text-center mb-4" data-mdb-ride="carousel">
                    <!-- Controls -->
                    <div class="d-flex justify-content-center mb-4 ">
                        <button class="carousel-control-prev position-relative" type="button" data-mdb-target="#carouselMultiItemExample" data-mdb-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next position-relative" type="button" data-mdb-target="#carouselMultiItemExample" data-mdb-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>
                    <!-- Inner -->
                    <div class="carousel-inner py-4">
                        <!-- Single item -->
                        <div class="carousel-item active">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="card">
                                            <img src="img/mv1.jpg" class="card-img-top" 840 × 840 alt="..." />
                                            <div class="card-body">
                                                <h5 class="card-title">Card title</h5>
                                                <p class="card-text">
                                                    Some quick example text to build on the card title and make up the bulk
                                                    of the card's content.
                                                </p>
                                                <a href="#!" class="btn btn-primary">Button</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 d-none d-lg-block">
                                        <div class="card">
                                            <img src="img/mv2.jpg" class="card-img-top" alt="..." />
                                            <div class="card-body">
                                                <h5 class="card-title">Card title</h5>
                                                <p class="card-text">
                                                    Some quick example text to build on the card title and make up the bulk
                                                    of the card's content.
                                                </p>
                                                <a href="#!" class="btn btn-primary">Button</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 d-none d-lg-block">
                                        <div class="card">
                                            <img src="img/mv3.jpg" class="card-img-top" alt="..." />
                                            <div class="card-body">
                                                <h5 class="card-title">Card title</h5>
                                                <p class="card-text">
                                                    Some quick example text to build on the card title and make up the bulk
                                                    of the card's content.
                                                </p>
                                                <a href="#!" class="btn btn-primary">Button</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Single item -->
                        <div class="carousel-item">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12">
                                        <div class="card">
                                            <img src="img/mv4.jpg" class="card-img-top" alt="..." />
                                            <div class="card-body">
                                                <h5 class="card-title">Card title</h5>
                                                <p class="card-text">
                                                    Some quick example text to build on the card title and make up the bulk
                                                    of the card's content.
                                                </p>
                                                <a href="#!" class="btn btn-primary">Button</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 d-none d-lg-block">
                                        <div class="card">
                                            <img src="img/mv5.jpg" class="card-img-top" alt="..." />
                                            <div class="card-body">
                                                <h5 class="card-title">Card title</h5>
                                                <p class="card-text">
                                                    Some quick example text to build on the card title and make up the bulk
                                                    of the card's content.
                                                </p>
                                                <a href="#!" class="btn btn-primary">Button</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 d-none d-lg-block">
                                        <div class="card">
                                            <img src="img/mv6.jpg" class="card-img-top" alt="..." />
                                            <div class="card-body">
                                                <h5 class="card-title">Card title</h5>
                                                <p class="card-text">
                                                    Some quick example text to build on the card title and make up the bulk
                                                    of the card's content.
                                                </p>
                                                <a href="#!" class="btn btn-primary">Button</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Single item -->
                        <div class="carousel-item">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12 mb-4 mb-lg-0">
                                        <div class="card">
                                            <img src="img/mv7.jpg" class="card-img-top" alt="..." />
                                            <div class="card-body">
                                                <h5 class="card-title">Card title</h5>
                                                <p class="card-text">
                                                    Some quick example text to build on the card title and make up the bulk
                                                    of the card's content.
                                                </p>
                                                <a href="#!" class="btn btn-primary">Button</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 mb-4 mb-lg-0 d-none d-lg-block">
                                        <div class="card">
                                            <img src="img/mv8.jpg" class="card-img-top" alt="..." />
                                            <div class="card-body">
                                                <h5 class="card-title">Card title</h5>
                                                <p class="card-text">
                                                    Some quick example text to build on the card title and make up the bulk
                                                    of the card's content.
                                                </p>
                                                <a href="#!" class="btn btn-primary">Button</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 mb-4 mb-lg-0 d-none d-lg-block">
                                        <div class="card">
                                            <img src="img/mv9.jpg" class="card-img-top" alt="..." />
                                            <div class="card-body">
                                                <h5 class="card-title">Card title</h5>
                                                <p class="card-text">
                                                    Some quick example text to build on the card title and make up the bulk
                                                    of the card's content.
                                                </p>
                                                <a href="#!" class="btn btn-primary">Button</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Inner -->
                </div>
            </div>

            <div class="container ">
                <h1>Mas reciente</h1>
                <div id="carouselMultiItemExample1" class="carousel slide carousel-dark text-center " data-mdb-ride="carousel">
                    <!-- Controls -->
                    <div class="d-flex justify-content-center mb-4 ">
                        <button class="carousel-control-prev position-relative" type="button" data-mdb-target="#carouselMultiItemExample1" data-mdb-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next position-relative" type="button" data-mdb-target="#carouselMultiItemExample1" data-mdb-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>
                    <!-- Inner -->
                    <div class="carousel-inner py-4 ">
                        <!-- Single item -->
                        <div class="carousel-item active">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="card">
                                            <img src="img/mr1.jpg" class="card-img-top" alt="..." />
                                            <div class="card-body">
                                                <h5 class="card-title">Card title</h5>
                                                <p class="card-text">
                                                    Some quick example text to build on the card title and make up the bulk
                                                    of the card's content.
                                                </p>
                                                <a href="#!" class="btn btn-primary">Button</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 d-none d-lg-block">
                                        <div class="card">
                                            <img src="img/mr2.jpg" class="card-img-top" alt="..." />
                                            <div class="card-body">
                                                <h5 class="card-title">Card title</h5>
                                                <p class="card-text">
                                                    Some quick example text to build on the card title and make up the bulk
                                                    of the card's content.
                                                </p>
                                                <a href="#!" class="btn btn-primary">Button</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 d-none d-lg-block">
                                        <div class="card">
                                            <img src="img/mr9.jpg" class="card-img-top" alt="..." />
                                            <div class="card-body">
                                                <h5 class="card-title">Card title</h5>
                                                <p class="card-text">
                                                    Some quick example text to build on the card title and make up the bulk
                                                    of the card's content.
                                                </p>
                                                <a href="#!" class="btn btn-primary">Button</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Single item -->
                        <div class="carousel-item">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12">
                                        <div class="card">
                                            <img src="img/mr3.jpg" class="card-img-top" alt="..." />
                                            <div class="card-body">
                                                <h5 class="card-title">Card title</h5>
                                                <p class="card-text">
                                                    Some quick example text to build on the card title and make up the bulk
                                                    of the card's content.
                                                </p>
                                                <a href="#!" class="btn btn-primary">Button</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 d-none d-lg-block">
                                        <div class="card">
                                            <img src="img/mr4.jpg" class="card-img-top" alt="..." />
                                            <div class="card-body">
                                                <h5 class="card-title">Card title</h5>
                                                <p class="card-text">
                                                    Some quick example text to build on the card title and make up the bulk
                                                    of the card's content.
                                                </p>
                                                <a href="#!" class="btn btn-primary">Button</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 d-none d-lg-block">
                                        <div class="card">
                                            <img src="img/mr5.jpg" class="card-img-top" alt="..." />
                                            <div class="card-body">
                                                <h5 class="card-title">Card title</h5>
                                                <p class="card-text">
                                                    Some quick example text to build on the card title and make up the bulk
                                                    of the card's content.
                                                </p>
                                                <a href="#!" class="btn btn-primary">Button</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Single item -->
                        <div class="carousel-item ">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12 mb-4 mb-lg-0">
                                        <div class="card">
                                            <img src="img/mr6.jpg" class="card-img-top" alt="..." />
                                            <div class="card-body">
                                                <h5 class="card-title">Card title</h5>
                                                <p class="card-text">
                                                    Some quick example text to build on the card title and make up the bulk
                                                    of the card's content.
                                                </p>
                                                <a href="#!" class="btn btn-primary">Button</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 mb-4 mb-lg-0 d-none d-lg-block">
                                        <div class="card">
                                            <img src="img/mr7.jpg" class="card-img-top" alt="..." />
                                            <div class="card-body">
                                                <h5 class="card-title">Card title</h5>
                                                <p class="card-text">
                                                    Some quick example text to build on the card title and make up the bulk
                                                    of the card's content.
                                                </p>
                                                <a href="#!" class="btn btn-primary">Button</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 mb-4 mb-lg-0 d-none d-lg-block">
                                        <div class="card">
                                            <img src="img/mr8.jpg" class="card-img-top " alt="..." />
                                            <div class="card-body">
                                                <h5 class="card-title">Card title</h5>
                                                <p class="card-text">
                                                    Some quick example text to build on the card title and make up the bulk
                                                    of the card's content.
                                                </p>
                                                <a href="#!" class="btn btn-primary">Button</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Inner -->
                </div>
            </div>
        <?php
        endif;
        ?>
        <!-- Carousel wrapper -->
    </main>
    <footer class="bg-primary text-center text-white border ">
        <!-- Grid container -->
        <div class="container p-4 pb-0 ">
            <!-- Section: Social media -->
            <section class="mb-4  ">
                <!-- Facebook -->
                <a class="btn btn-primary btn-floating m-1" style="background-color: #3b5998;" href="#!" role="button"><i class="fab fa-facebook-f"></i></a>

                <!-- Twitter -->
                <a class="btn btn-primary btn-floating m-1" style="background-color: #55acee;" href="#!" role="button"><i class="fab fa-twitter"></i></a>

                <!-- Google -->
                <a class="btn btn-primary btn-floating m-1" style="background-color: #dd4b39;" href="#!" role="button"><i class="fab fa-google"></i></a>

                <!-- Instagram -->
                <a class="btn btn-primary btn-floating m-1" style="background-color: #ac2bac;" href="#!" role="button"><i class="fab fa-instagram"></i></a>

                <!-- Linkedin -->
                <a class="btn btn-primary btn-floating m-1" style="background-color: #0082ca;" href="#!" role="button"><i class="fab fa-linkedin-in"></i></a>

            </section>
            <!-- Section: Social media -->
        </div>
        <!-- Grid container -->

        <!-- Copyright -->
        <div class="text-center p-3 " style="background-color: rgba(0, 0, 0, 0.2);">
            © 2020 Copyright:
            <a class="text-white" href="https://mdbootstrap.com/">MDBootstrap.com</a>
        </div>
        <!-- Copyright -->
    </footer>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"></script>
</body>

</html>